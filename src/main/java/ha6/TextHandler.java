package ha6;

public interface TextHandler {
	public abstract void interpretText(String[] words);
	
	public abstract void setNextHandler(TextHandler nextHandler);
	
	public abstract void setEditor(Editor editor);
}
