package ha6;

import java.util.Iterator;

import javafx.scene.shape.Line;

public class LineHandler implements TextHandler{

	TextHandler nextHandler;
	Editor editor;
	
	@Override
	public void interpretText(String[] words) {
		if(words[0].toLowerCase().equals("line")) {
			Iterator<Drawings> it = editor.myStack.iterator();
			while(it.hasNext()) {
				Drawings drawing = it.next();
				if(drawing.getName().equals(words[1])) {
					return;
				}
			}
			CustomLine line = LineFactory.createLine(words[1],Double.parseDouble(words[2]), Double.parseDouble(words[3]), Double.parseDouble(words[4]), Double.parseDouble(words[5]),editor);
			line.draw();
			editor.myStack.push(line);
		}
		else {
			nextHandler.interpretText(words);
		}
	}

	@Override
	public void setNextHandler(TextHandler nextHandler) {
		this.nextHandler = nextHandler;
		
	}

	@Override
	public void setEditor(Editor editor) {
		this.editor = editor;
		
	}

}
