package ha6;

public class DrawingsFactory {
	public static Drawings createDrawable(String type) {
		if(type.equals("Line")) {
			return new CustomLine(null, null, null, null, null, null);
		}else if(type.equals("Group")){
			return new CustomGroup();
		}
		return null;
	}
}
