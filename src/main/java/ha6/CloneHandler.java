package ha6;

import java.util.Iterator;

public class CloneHandler implements TextHandler{
	
	private Editor editor;
	private TextHandler nextHandler;
	
	
	@Override
	public void interpretText(String[] words) {
		CustomGroup group = null;
		if(words[0].toLowerCase().equals("clone")) {
			//Find Group
			Iterator<Drawings> it = editor.myStack.iterator();
			while(it.hasNext()) {
				Drawings drawing = it.next();
				if(drawing.getName().equals(words[1])) {
					group = (CustomGroup)drawing;
				}
			}
		}
		//Create new Group
		CustomGroup group2 = GroupFactory.createGroup(words[2]);
		for(Drawings drawing: group.getDrawings()) { //Copy Content
			group2.addDrawings(drawing.copyReturn(Double.parseDouble(words[3]), Double.parseDouble(words[4])));
		}
		group2.draw();
		group2.setClone(true);
		editor.myStack.push(group2);
		
		
	}

	@Override
	public void setNextHandler(TextHandler nextHandler) {
		this.nextHandler = nextHandler;
		
	}

	@Override
	public void setEditor(Editor editor) {
		this.editor = editor;
		
	}

}
