package ha6;

public interface Drawings {
	public void draw();
	
	public void undoDraw();
	
	public String getName();
	
	public void move(double xOffset,double yOffset);
	
	public Drawings copyReturn(double xOffset,double yOffset);
}
