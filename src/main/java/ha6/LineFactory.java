package ha6;

public class LineFactory extends DrawingsFactory{
	
	public static CustomLine createLine(String name,double sX,double sY,double eX,double eY,Editor editor) {
		return new CustomLine(name,sX,sY,eX,eY,editor);
	}

}
