package ha6;

import java.util.ArrayList;
import java.util.Iterator;

import org.sdmlib.models.debug.FlipBook;
import org.sdmlib.serialization.SDMLibJsonIdMap;

import javafx.scene.shape.Line;


public class CustomGroup implements Drawings{
	private String name;
	private ArrayList<Drawings> myDrawings = new ArrayList<Drawings>();
	private boolean clone;

	@Override
	public void draw() {
		for(Drawings drawings:myDrawings) {
			drawings.draw();
		}
	}
	
	public CustomGroup() {
		
	}
	
	public CustomGroup(String name) {
		this.name = name;
	}
	
	public void addDrawings(Drawings drawings) {
		myDrawings.add(drawings);
	}

	@Override
	public void undoDraw() {
		if(clone == true) {
			for(Drawings drawings:myDrawings) {
				drawings.undoDraw();
			}
		}else {
			for(int i = 0; i < myDrawings.size();i++)
			myDrawings.remove(i);
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	public ArrayList<Drawings> getDrawings() {
		return myDrawings;
	}

	@Override
	public void move(double xOffset, double yOffset) {
		for(Drawings drawing:myDrawings) {
			drawing.move(xOffset, yOffset);
		}
		
	}

	@Override
	public Drawings copyReturn(double xOffset, double yOffset) {
		CustomGroup group = GroupFactory.createGroup("Cloned Group");
		for(Drawings drawing:myDrawings) {
			group.addDrawings(drawing.copyReturn(xOffset, yOffset));
		}
		return group;
	}

	public boolean isClone() {
		return clone;
	}

	public void setClone(boolean clone) {
		this.clone = clone;
	}
	
	
}
