package ha6;


import java.util.Iterator;

import javafx.scene.Group;

public class GroupHandler implements TextHandler{

	
	TextHandler nextHandler;
	Editor editor;
	
	
	@Override
	public void interpretText(String[] words) {
		if(words[0].toLowerCase().equals("group")) {
			CustomGroup test = new CustomGroup();
			Iterator <Drawings> it = editor.myStack.iterator();
			while(it.hasNext()) {
				Drawings drawing = it.next();
				if(drawing.getName().equals(words[1])) {
					return;
				}
			}
			for(int i = 2; i<words.length;i++) {
				it = editor.myStack.iterator();
				while(it.hasNext()){
					Drawings drawing = it.next();
					if(drawing.getName().equals(words[i])) {
						test.addDrawings(drawing);
						continue;
					}
					
				}
				
			}
			test.setName(words[1]);
			test.setClone(false);
			editor.myStack.push(test);
		}
		else {
			nextHandler.interpretText(words);
		}
		
	}

	@Override
	public void setNextHandler(TextHandler nextHandler) {
		this.nextHandler = nextHandler;
		
	}

	@Override
	public void setEditor(Editor editor) {
		this.editor = editor;
		
	}


}
