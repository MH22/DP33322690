package ha6;

public class CustomLine extends javafx.scene.shape.Line implements Drawings{
	private String id;
	private Editor editor;
	
	public CustomLine(String id,Double x,Double y,Double z, Double p,Editor editor) {
		super(x,y,z,p);
		this.id = id;
		this.editor = editor;
	}
	
	@Override
	public void draw() {
		editor.root.getChildren().add(this);
	}
	
	public void setEditor(Editor editor) {
		this.editor = editor;
	}

	@Override
	public void undoDraw() {
		if(editor.root.getChildren().contains(this)) {
			editor.root.getChildren().remove(this);
		}
		
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void move(double xOffset, double yOffset) {
		this.setEndX(this.getEndX()+xOffset);
		this.setEndY(this.getEndY()+yOffset);
		this.setStartX(this.getStartX()+xOffset);
		this.setStartY(this.getStartY()+yOffset);
		
	}
	
	public Drawings copyReturn(double xOffset,double yOffset) {
		CustomLine temp = LineFactory.createLine("Cloned Line",this.getStartX()+xOffset,this.getStartY()+yOffset,this.getEndX()+xOffset,this.getEndY()+yOffset,editor);
		return temp;
	}
}
