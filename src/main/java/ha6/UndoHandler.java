package ha6;

public class UndoHandler implements TextHandler{

	TextHandler nextHandler;
	Editor editor;
	
	@Override
	public void interpretText(String[] words) {
		if(words[0].toLowerCase().equals("undo")) {
			editor.myStack.pop().undoDraw();
		}else {
			nextHandler.interpretText(words);
		}
		
	}

	@Override
	public void setNextHandler(TextHandler nextHandler) {
		this.nextHandler = nextHandler;
		
	}

	@Override
	public void setEditor(Editor editor) {
		this.editor = editor;
		
	}

}
