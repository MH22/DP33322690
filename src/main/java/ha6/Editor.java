package ha6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.sdmlib.models.debug.FlipBook;
import org.sdmlib.serialization.SDMLibJsonIdMap;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Line;

public class Editor extends Application{
	
	public GraphicsContext gc;
	Group root;
	int numOfChilds;
	public Stack<Drawings> myStack = new Stack<Drawings>();
	public HashMap<String,CustomLine> myLines = new HashMap<String,CustomLine>();
	public TextField text;
	public FlipBook flipBook;
	public static void main(String[] args) {
        launch(args);
    }
	private TextHandler handler = new LineHandler();
 
    @Override
    public void start(Stage primaryStage) {
    	
        primaryStage.setTitle("Test");
        root = new Group();
        Canvas canvas = new Canvas(900, 900);
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(2);
        text = new TextField();
        text.setId("text");
        
        
		
        
        root.getChildren().add(canvas);
        text.setAlignment(Pos.BOTTOM_CENTER);
        root.getChildren().add(text);
        
        text.setOnKeyPressed(event -> draw(event.getCode(),text));
        numOfChilds = root.getChildren().size();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        
        CloneHandler cloneHandler = new CloneHandler();
        cloneHandler.setEditor(this);
        
        UndoHandler undoHandler = new UndoHandler();
    	undoHandler.setEditor(this);
    	undoHandler.setNextHandler(cloneHandler);
        
    	GroupHandler groupHandler = new GroupHandler();
    	groupHandler.setNextHandler(undoHandler);
    	groupHandler.setEditor(this);

    	handler.setNextHandler(groupHandler);
    	handler.setEditor(this);
    	
    	SDMLibJsonIdMap t = new SDMLibJsonIdMap();
    	t.add(root.getChildren());
    	flipBook = t.createFlipBook();
    	flipBook.setReading(true);
    	
    }

	private void draw(KeyCode key,TextField textField) {
		if(KeyCode.ENTER == key) {
			String text = textField.getText();
			textField.clear();
			String[] words = text.split(" ");
			handler.interpretText(words);
		}
		
	}

	
}
