package ha4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
/**
 * HA4: 10/10
 */
public class TimeListener {
	public static void main(String[]args) {
		Thread writeToFile = new Thread(new Runnable() {

			@Override
			public void run() {
				File t = new File("."+File.separator+"time.txt");
				try {
					while (true) {
						FileWriter f = new FileWriter(t, false);
						f.write(new Date().toString());
						f.close();
						TimeUnit.SECONDS.sleep(2);
					}
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}

			}
		}) ;
		writeToFile.start();
		
		
		//Creating Watcher
		Thread observeDirectory = new Thread(new Runnable() {
			@Override
			public void run() {
				Path path = Paths.get(".");
				try {
					WatchService myWatcher = path.getFileSystem().newWatchService();
					path.register(myWatcher, ENTRY_MODIFY);
					
					while(true) {
						WatchKey key = myWatcher.take(); 
						Thread.sleep(10);// Wait for all to update
						List<WatchEvent<?>> list = key.pollEvents();
						
						for(WatchEvent<?> event:list) {
							if(event.kind() == OVERFLOW) {
								System.out.println("Error");
								continue;
							}else {
								Path file = (Path) event.context();
								if(file.toString().endsWith(".txt")) { // Check if File is txt
									System.out.println(java.nio.file.Files.readAllLines(file));
								}else {
									System.out.println("no TXT");
									System.out.println(file.toString());
								}
							}
						}key.reset();
					}
				} catch (IOException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		observeDirectory.start();
		
		try {
			observeDirectory.join();
			writeToFile.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

