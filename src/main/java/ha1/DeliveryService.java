package ha1;

public class DeliveryService {
private Caterer caterer;
	
public DeliveryService(){
	this.caterer = new PizzaShop();
}

public DeliveryService(Caterer caterer){
	this.caterer = caterer;
}

public void deliver(int foodNo,String address) {
	caterer.deliver(foodNo,address);
}

public void setCaterer(Caterer caterer) {
	this.caterer = caterer;
}
}
