package ha5;


import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONObject;
import org.json.JSONString;

public class TaskProxy implements TaskInterface{
	MqttClient myClient;
	String name;
	String owner;
	String dest;
	String loc;
	
	public void setClient(MqttClient myClient) {
		this.myClient = myClient;
	}
		
	public TaskProxy(String packetName,String owner,String dest,String loc, MqttClient myClient){
		this.name = packetName;
		this.owner = owner;
		this.dest = dest;
		this.loc = loc;
		JSONObject json = new JSONObject().put("NEW", packetName)
				.put("owner",owner)
				.put("dest", dest)
				.put("loc",loc);
		try {
			myClient.publish(ClientController.getTopic(), new MqttMessage(json.toString().getBytes()));
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void send (String from,String to) {
		JSONObject json = new JSONObject().put("kind", "send")
				.put("to", to)
				.put("from", from);
	}

	@Override
	public void setDestination(String dest,String owner) {
		JSONObject json = new JSONObject().put("dest", dest);
		try {
			myClient.publish(ClientController.getTopic(), new MqttMessage(json.toString().getBytes()));
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void setLocation(String loc,String owner) {
		JSONObject json = new JSONObject().put("loc", loc);
		try {
			myClient.publish(ClientController.getTopic(), new MqttMessage(json.toString().getBytes()));
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public String getDest() {
		
		return dest;
	}

	@Override
	public String getLoc() {
		
		return null;
	}
}
