package ha5;

import java.io.FileInputStream;
	import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javafx.application.*;
	import javafx.fxml.*;
	import javafx.scene.layout.*;
	import javafx.scene.*;
	import javafx.stage.*;
	import javafx.fxml.*;
	import javafx.scene.control.Button;
	import javafx.scene.control.ComboBox;
	import javafx.collections.*;

@SuppressWarnings("restriction")
public class Client extends Application{	

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		// Create the FXMLLoader
		FXMLLoader loader = new FXMLLoader();
		// Path to the FXML File
		String fxmlDocPath = "src/main/java/ha5/Client.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
		// Create the Pane and all Details
		AnchorPane root = (AnchorPane) loader.load(fxmlStream);
		ClientController controller = loader.getController();
		// Create the Scene
		Scene scene = new Scene(root);
		// Set the Scene to the Stage
		stage.setScene(scene);
		stage.setResizable(false);
		// Set the Title to the Stage
		stage.setTitle("Client");		
		// Display the Stage
		stage.show();
	}

}
