package ha5;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ResourceBundle;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.fxml.*;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.collections.*;
import javafx.scene.text.*;
import javafx.scene.control.*;

public class ClientController implements Initializable{
	
@FXML
private ComboBox<String> start;

@FXML
private ComboBox<String> destination;

@FXML
private Button sendButton;

@FXML
private Button disconnect;

@FXML
private Text status;

@FXML
private TextField packageName;

private ObservableList<String> cities = FXCollections.observableArrayList("Berlin","Kassel","Wattenbach","Hamburg");

private MqttClient MQTTClient;

private final static String TOPIC = "mh996";
private String myID = MQTTClient.generateClientId();

private boolean usable = true;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		start.setItems(cities);
		destination.setItems(cities);
		status.setText("---"); 
		destination.getSelectionModel().selectFirst();
		start.getSelectionModel().selectFirst();
		
		try {
			MQTTClient = new MqttClient("tcp://test.mosquitto.org:1883", myID);
			MQTTClient.setCallback(new MqttCallback() {

				@Override
				public void connectionLost(Throwable cause) {
					System.out.println("Lost Connection");
					
				}

				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					if(message.toString().equals("Unterwegs")) {
						status.setText("Unterwegs");
					}
					if(message.toString().equals("Abgeliefert")) {
						status.setText("Done");
					}
					
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
					// TODO Auto-generated method stub
					
				}
				
			});
			MQTTClient.connect();
			MQTTClient.subscribe(myID);
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void disconnect() {
		try {
			MQTTClient.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createTask() {
		if(packageName.getText()!=null && packageName.getText()!=""&& usable == true) {
			System.out.println(packageName.getText().toString());
			TaskInterface r = new TaskProxy(packageName.getText(),myID,destination.getSelectionModel().getSelectedItem(),start.getSelectionModel().getSelectedItem(),MQTTClient);
			status.setText("Auftrag geschickt");
			usable = false;
		}
		
	}
	
	public MqttClient getMessenger() {
		return MQTTClient;
	}
	
	public static String getTopic() {
		return TOPIC;
	}

}
