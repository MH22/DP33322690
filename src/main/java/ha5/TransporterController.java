package ha5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.collections.*;
import javafx.scene.text.*;
import javafx.scene.control.cell.*;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.json.JSONObject;
import org.json.JSONStringer;

public class TransporterController implements Initializable{

	private final static String TOPIC = "mh996";
	
	@FXML
    private TableView <Task>berlinColumn;

    @FXML
    private TableView <Task>hamburgColumn;

    @FXML
    private TableView <Task> wattenbachColumn;

    @FXML
    private TableView <Task>kasselColumn;

    @FXML
    private Text currentTown;
    
    @FXML
    private Button disconnect;
    
    @FXML
    private Button moveBtn;
    
    @FXML
    private TableColumn<Task, String> haCol;

    @FXML
    private TableColumn<Task, String> ksCol;
    
    @FXML
    private TableColumn<Task, String> waCol;
    
    @FXML
    private TableColumn<Task, String> berCol;
    
    private MqttClient MQTTClient;
    
    private ArrayList<Task> tasks = new ArrayList<Task>();
    
    private int location = 0;
    
    
	private ObservableList<Task> kasselList = FXCollections.observableArrayList();
    private ObservableList<Task> wabaList = FXCollections.observableArrayList();
    private ObservableList<Task> berList = FXCollections.observableArrayList();
    private ObservableList<Task> hamList = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		haCol.setCellValueFactory(new PropertyValueFactory<Task,String>("StringName"));
		waCol.setCellValueFactory(new PropertyValueFactory<Task,String>("StringName"));
		ksCol.setCellValueFactory(new PropertyValueFactory<Task,String>("StringName"));
		berCol.setCellValueFactory(new PropertyValueFactory<Task,String>("StringName"));
		
		wattenbachColumn.setItems(wabaList);
		kasselColumn.setItems(kasselList);
		berlinColumn.setItems(berList);
		hamburgColumn.setItems(hamList);
	
		
		try {
			MQTTClient = new MqttClient("tcp://test.mosquitto.org:1883", MqttClient.generateClientId());
			MQTTClient.setCallback(new MqttCallback() {

				@Override
				public void connectionLost(Throwable cause) {
					System.out.println(cause);
				}

				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					System.out.println(topic+" "+message);
					JSONObject t = new JSONObject(message.toString());
					if(t.has("NEW")){
						tasks.add(new Task(t.getString("NEW"),t.getString("owner"),t.getString("dest"),t.getString("loc")));
						updateGui();
					}
					
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
					
				}
				
			});
			MQTTClient.connect();
			MQTTClient.subscribe("mh996");
			//MQTTClient.publish("mh996", new MqttMessage("hello".getBytes()));
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void disconnecting() {
		try {
			if(MQTTClient.isConnected()) {
				MQTTClient.disconnect();
			}
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void updateGui() {
		System.out.println("Update gui");
		for(Task temp : tasks) {
			if(temp.getLoc().equals("Wattenbach")&&!wabaList.contains(temp)&&temp.getTaken()==false) {
				wabaList.add(temp);
				System.out.println(wabaList);
				wattenbachColumn.refresh();
			}
			if(temp.getLoc().equals("Hamburg")&&!hamList.contains(temp)&&temp.getTaken()==false) {
				hamList.add(temp);
				System.out.println(hamList);
				hamburgColumn.refresh();
			}
			if(temp.getLoc().equals("Berlin")&&!berList.contains(temp)&&temp.getTaken()==false) {
				berList.add(temp);
				System.out.println(berList);
				berlinColumn.refresh();;
			}
			if(temp.getLoc().equals("Kassel")&&!kasselList.contains(temp)&&temp.getTaken()==false) {
				kasselList.add(temp);
				System.out.println(kasselList);
				kasselColumn.refresh();
			}
			
		}
		
	}
	
	public void move() throws MqttPersistenceException, MqttException {
		
		if(tasks.isEmpty()) {
			return;
		}
		location++;
		if(location == 1) {
			currentTown.setText("Wattenbach");
			for(Iterator<Task> it = tasks.iterator(); it.hasNext();) {
				Task temp = it.next();
				if(temp.getLoc().equals("Wattenbach")) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Unterwegs".getBytes()));
					wabaList.remove(temp);
					temp.setTaken(true);
				}
				
				if(temp.getDest().equals("Wattenbach")&&temp.getTaken() == true) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Abgeliefert".getBytes()));
					it.remove();
				}
				wattenbachColumn.refresh();
			}
		}
		if (location == 2) {
			currentTown.setText("Berlin");
			for(Iterator<Task> it = tasks.iterator(); it.hasNext();) {
				Task temp = it.next();
				if(temp.getLoc().equals("Berlin")) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Unterwegs".getBytes()));
					berList.remove(temp);
					temp.setTaken(true);
				}
				if(temp.getDest().equals("Berlin")&&temp.getTaken() == true) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Abgeliefert".getBytes()));
					it.remove();
				}
				
			}
			berlinColumn.refresh();
		}
		if(location == 3) {
			currentTown.setText("Kassel");
			for(Iterator<Task> it = tasks.iterator(); it.hasNext();) {
				Task temp = it.next();
				if(temp.getLoc().equals("Kassel")) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Unterwegs".getBytes()));
					kasselList.remove(temp);
					temp.setTaken(true);
				}
				if(temp.getDest().equals("Kassel")&&temp.getTaken() == true) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Abgeliefert".getBytes()));
					it.remove();
				}
			}
			kasselColumn.refresh();
		}
		if(location == 4) {
			currentTown.setText("Hamburg");
			for(Iterator<Task> it = tasks.iterator(); it.hasNext();) {
				Task temp = it.next();
				if(temp.getLoc().equals("Hamburg")) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Unterwegs".getBytes()));
					hamList.remove(temp);
					temp.setTaken(true);
				}
				if(temp.getDest().equals("Hamburg")&&temp.getTaken() == true) {
					MQTTClient.publish(temp.getOwner(), new MqttMessage("Abgeliefert".getBytes()));
					it.remove();
				}
			}
			hamburgColumn.refresh();
			location = (location/4)-1;
		}
		
	}
}
