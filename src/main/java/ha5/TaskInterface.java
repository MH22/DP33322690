package ha5;

public interface TaskInterface {
	
	public void setDestination(String dest,String owner);
	
	public void setLocation(String loc,String owner);
	
	public String getDest();
	
	public String getLoc();
}
