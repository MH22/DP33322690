package ha5;


import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.collections.*;
import javafx.scene.text.*;
import javafx.scene.control.cell.*;
import javafx.beans.property.SimpleStringProperty;


public class Task implements TaskInterface{

	private String destination;
	private String location;
	private String name;
	private String owner;
	private boolean taken = false;
	public SimpleStringProperty nameString = new SimpleStringProperty();
	
	public Task(String packetName, String owner,String dest,String loc) {
		name = packetName;
		this.owner = owner;
		this.destination = dest;
		this.location = loc;
		nameString.set(name);
	}
	
	@Override
	public void setDestination(String dest,String owner) {
		destination = dest;
		
	}

	@Override
	public void setLocation(String loc,String owner) {
		location = loc;
		
	}

	@Override
	public String getDest() {
		// TODO Auto-generated method stub
		return destination;
	}

	@Override
	public String getLoc() {
		// TODO Auto-generated method stub
		return location;
	}
	
	public String getName() {
		return name;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setTaken(boolean taken) {
		this.taken = taken;
	}
	
	public boolean getTaken() {
		return taken;
	}
	
	public String getStringName() {
		return nameString.get();
	}

}
