package ha5;

import java.io.FileInputStream;
	import java.io.IOException;
	import javafx.application.Application;
	import javafx.fxml.FXMLLoader;
	import javafx.scene.Scene;
	import javafx.scene.layout.*;
	import javafx.stage.Stage;

	@SuppressWarnings("restriction")
public class Transporter extends Application{	
	public static void main(String[] args) {
		Application.launch(args);
	}
		     
	@Override
	public void start(Stage stage) throws IOException {
		// Create the FXMLLoader
		FXMLLoader loader = new FXMLLoader();
		// Path to the FXML File
		String fxmlDocPath = "src/main/java/ha5/Transporter.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
		// Create the Pane and all Details
		AnchorPane root = (AnchorPane) loader.load(fxmlStream);
		TransporterController controller = loader.getController();
		// Create the Scene
		Scene scene = new Scene(root);
		// Set the Scene to the Stage
		stage.setScene(scene);
		stage.setResizable(false);
		// Set the Title to the Stage
		stage.setTitle("Transporter");
		// Display the Stage
		stage.show();
	}
	
}
