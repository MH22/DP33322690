package ha2;

import java.util.ArrayList;

public class Fachgebiet extends Unit {
	private ArrayList<Personen> persons = new ArrayList<Personen>(); 
	
	public Fachgebiet(String name) {
		this.name = name;
	}
	
	public void addPerson(Personen person) {
		persons.add(person);
	}
	
	@Override
	public int countMembers() {
		return persons.size();
	}
	
}
