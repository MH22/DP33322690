package ha2;

public class Fachbereich extends Unit {
	String dekanat;
	Personen dekan;
	
	public Fachbereich(String name) {
		this.name = name;
	}
	
	public void setDekan(Personen dekan) {
		this.dekan = dekan;
	}
	
	public Personen getDekan() {
		return dekan;
	}
}
