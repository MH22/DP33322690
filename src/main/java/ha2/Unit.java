package ha2;

import java.util.ArrayList;

public abstract class Unit {
	protected ArrayList<Unit> kids = new ArrayList<Unit>();
	String name;
	
	public int countMembers() {
		int z = 0;
		for(Unit unit:kids) {
			z+=unit.countMembers();
		}
		return z;
	}
	
	public void add(Unit unit) {
		kids.add(unit);
	}
	
	public void remove(Unit unit) {
		kids.remove(unit);
	}
}
