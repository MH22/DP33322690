package ha3;

public class ConstantHandler extends MainHandler{

	@Override
	public boolean doAction(String instruction) {
		if(instruction.startsWith("Ldc")) {
			Interpreter.myStack.push(Integer.parseInt(instruction.split(" ")[1]));
			return true;
		}
		return false;
	}

}
