package ha3;

public class VariableHandler extends MainHandler{

	@Override
	public boolean doAction(String instruction) {
		if(instruction.startsWith("Ld")) {
			String var = instruction.split(" ")[1];
			int number = Interpreter.register.get(var);
			Interpreter.myStack.push(number);
			return true;
		}
		return false;
	}

}
