package ha3;

public class MultHandler extends MainHandler{

	@Override
	public boolean doAction(String instruction) {
		if(instruction.startsWith("Mult")) {
			int var = Interpreter.myStack.pop()*Interpreter.myStack.pop();
			Interpreter.myStack.push(var);
			return true;
		}
		return false;
	}

}
