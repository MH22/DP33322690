package ha3;

public class OutputHandler extends MainHandler{

	@Override
	public boolean doAction(String instruction) {
		if(instruction.startsWith("Print")) {
			System.out.println(Interpreter.myStack.pop());
			return true;
		}
		return false;
	}

}
