package ha3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Interpreter {
	public final static Stack<Integer> myStack = new Stack<Integer>();
	public final static HashMap<String,Integer> register = new HashMap<String,Integer>();
	public ArrayList<MainHandler> myHandlers = new ArrayList<MainHandler>();
	
	public void readText(String text) {
		String[] lines = text.split("\n");
		for(String line:lines) {
			interpretLine(line);
		}
	}
	
	private void interpretLine(String line) {
		for(MainHandler handler:myHandlers) {
			if(handler.doAction(line)==true) {
				return;
			};
		}
	}
}
