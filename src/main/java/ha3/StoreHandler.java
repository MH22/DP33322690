package ha3;

public class StoreHandler extends MainHandler {

	@Override
	public boolean doAction(String instruction) {
		if(instruction.startsWith("Store")) {
			Interpreter.register.put(instruction.split(" ")[1], Interpreter.myStack.pop());
			return true;
		}
		return false;
	}

}
