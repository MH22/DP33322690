package ha7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import org.apache.batik.util.Platform;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONString;
import org.sdmlib.models.debug.FlipBook;
import org.sdmlib.serialization.SDMLibJsonIdMap;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.input.KeyCode;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class GitWatcher extends Application implements MqttCallback{

	private final VBox root = new VBox();
	private Button addCrawler = new Button();
	public TextField textField;
	private HashMap<String,TextArea> textAreaMap = new HashMap<String,TextArea>();
	private MqttClient mqttClient;
	private String mqttUrl = "tcp://avocado.uniks.de:41883";
	private ScrollPane myPane;
	
	public static void main(String[] args) {
        launch(args);
    }

	@Override
	public void start(Stage arg0){

		arg0.setWidth(800);
		arg0.setHeight(800);		
		root.prefWidth(800);
		root.prefHeight(800);
		root.setPadding(new Insets(10));
	    root.setSpacing(5);
		textField = new TextField();
		textField.setText("Enter URL to start additional GitCrawler");
		textField.setOnKeyPressed(event -> draw(event.getCode(),textField));
		try {
			mqttClient = new MqttClient(mqttUrl,""+System.currentTimeMillis());
			mqttClient.connect();
			mqttClient.setCallback(this);
			mqttClient.subscribe("uniks.GitGrabberApp");
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		arg0.setResizable(false);
		textField.setAlignment(Pos.BOTTOM_CENTER);
		root.getChildren().add(textField);

		
		arg0.setScene(new Scene(root));
		arg0.show();
	}


	private Object draw(KeyCode code, TextField textField2) {
		if(KeyCode.ENTER.equals(code)) {
			String url = textField2.getText();
			GitCrawler temp = new GitCrawler();
			temp.start(new String[] {url});
			
		}
		return null;
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) {
		byte[] payload = message.getPayload();
		String msg = new String(payload);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = gson.fromJson(msg, JsonObject.class);
		String gitUrl = jsonObject.get("gitUrl").getAsString();
		System.out.println(gitUrl+" arrived");
		
		TextArea textArea = textAreaMap.get(gitUrl);
		
		if(textArea == null) {
			TextArea newTextArea = new TextArea();
			newTextArea.setText(msg);
			textAreaMap.put(gitUrl, newTextArea);
			javafx.application.Platform.runLater(()-> run(newTextArea));
		}else {
			System.out.println("No such thing");
			textArea.setText(msg);
		}
		
		
		
		
	}

	private Object run(TextArea newTextArea) {
		root.getChildren().add(newTextArea);
		return null;
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

}
