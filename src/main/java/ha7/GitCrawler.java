package ha7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class GitCrawler implements MqttCallback{
	private ScheduledExecutorService crawlExecutor;
	private String gitURL;
	private String mqttUrl = "tcp://avocado.uniks.de:41883";
	private MqttClient mqttClient;
	private JsonObject latest;
	
	
	public static void main(String[] args) {
		new GitCrawler().start(args);
	}
	
	public void start(String[] args) {
		
		crawlExecutor = Executors.newSingleThreadScheduledExecutor();
		crawlExecutor.execute(()->doCrawl("repeat"));
		
		try {
			if(args.length == 0) {
				gitURL = "https://gitlab.com/MH22/DP33322690/commits/master";
			}else {
				gitURL = args[0];
				if(args.length>1) {
					mqttUrl = args[1];
				}
			}
			
			Logger.getGlobal().info("crawling "+gitURL+" reporting to:"+ mqttUrl +"Pubishing on Topic: uniks.GitGrabberApp");
			
			connect();
			
			
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void connect() throws MqttException {
		if(mqttClient != null&&mqttClient.isConnected()) {
			mqttClient.close();
		}
		mqttClient = new MqttClient(mqttUrl,gitURL+System.currentTimeMillis());
		mqttClient.setCallback(this);
		mqttClient.connect();
		mqttClient.subscribe("uniks.GitCrawler");
		mqttClient.subscribe("uniks.GitWatcherApp");
		
	}

	private int doCrawl(String string) {
		try {
			if(gitURL == null || (!gitURL.startsWith("http"))){
				System.out.println("wrong url");
				return -1;
			}
			URL url = new URL(gitURL);
			
			URLConnection connection = url.openConnection();
			BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));
			String inputLine;
			String state = "searching";
			JsonObject commitObj = new JsonObject();
			commitObj.addProperty("gitUrl", gitURL);
			System.out.println(gitURL);
			
			while((inputLine = in.readLine()) != null) {
				if(state.equals("searching")&&inputLine.indexOf("<li class=\"commit ")>=0) {
					state = "search message";
					System.out.println("start v3");
				}
				if(state.equals("search message")&&inputLine.indexOf("<a class=\"commit-row-message item-title")>=0) {
					int startPos = inputLine.indexOf("\">");
					int endPos = inputLine.indexOf("</a>",startPos);
					String message = inputLine.substring(startPos+2, endPos);
					commitObj.addProperty("message", message);
					state = "search Author";
					System.out.println(message);
				}
				if(state.equals("search Author")&&inputLine.indexOf("<a class=\"commit-author-link has-tooltip")>=0) {
					int startPos = inputLine.indexOf(">");
					int endPos = inputLine.indexOf("</a>");
					String message = inputLine.substring(startPos+1,endPos);
					commitObj.addProperty("author", message);
					System.out.println(message);
					state = "search Time";
				}
				if(state.equals("search Time")&&inputLine.indexOf("<time class")>=0) {
					int startPos = inputLine.lastIndexOf("title=\"")+7;
					int endPos = inputLine.indexOf(" datetime")-1;
					String message = inputLine.substring(startPos,endPos);
					commitObj.addProperty("commitTime", message);
					Date today = new Date();
					commitObj.addProperty("crawlTime",today.toString()); 
					state = "done";
				}
				if(state.equals("done")) {
					break;
				}
			}
			in.close();
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String json = gson.toJson(commitObj) + "\n";
			
			MqttMessage message = new MqttMessage(json.getBytes());
			mqttClient.publish("uniks.GitGrabberApp", message);
			
		} catch (IOException | MqttException e) {
			try {
				connect();
			} catch (MqttException e1) {
				
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			System.out.println("Done");
			if(string != null && string.equals("repeat")) {
				crawlExecutor.schedule(()->doCrawl("repeat"), 4, TimeUnit.SECONDS);
			}
		}
		return 0;
	}

	@Override
	public void connectionLost(Throwable cause) {
		boolean done = false;
		
		while(!done) {
			try {
				if(mqttClient.isConnected() == true) {
					done = true;
				}else{
					mqttClient.reconnect();
				}
			} catch (MqttException e) {
				System.out.println("reconnect failed");
				//e.printStackTrace();
			}
			if(mqttClient.isConnected() == true) {
				done = true;
			}
			try {
				System.out.println("reconnecting...");
				Thread.sleep(10000);
				
			} catch (InterruptedException e) {
				System.out.println("Interruption");
				//e.printStackTrace();
			}
		}
		
		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}
	
	
}
