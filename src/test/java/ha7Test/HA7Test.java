package ha7Test;

import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import ha6.Editor;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;

public class HA7Test extends ApplicationTest{
	
	private ha7.GitWatcher gitWatcher;
	
	
	@Override
	public void start(Stage primaryStage) {
    	gitWatcher = new ha7.GitWatcher();
    	gitWatcher.start(primaryStage);
	}  
	
	@Test
	public void test() {
		System.out.println("start");
		clickOn(gitWatcher.textField);
		gitWatcher.textField.clear();
		write("https://gitlab.com/MH22/DP33322690/commits/master").push(KeyCode.ENTER);
	}
}
