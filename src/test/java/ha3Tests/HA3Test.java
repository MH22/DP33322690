package ha3Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import ha3.ConstantHandler;
import ha3.Interpreter;
import ha3.MultHandler;
import ha3.OutputHandler;
import ha3.StoreHandler;
import ha3.VariableHandler;
import junit.framework.Assert;
/**
 * HA3: 10/10
 */
public class HA3Test {
	
	@Test
	public void Test() {
		//Initializing
		Interpreter myInterpreter = new Interpreter();
		myInterpreter.myHandlers.add(new OutputHandler());
		myInterpreter.myHandlers.add(new StoreHandler());
		myInterpreter.myHandlers.add(new ConstantHandler());
		myInterpreter.myHandlers.add(new MultHandler());
		myInterpreter.myHandlers.add(new VariableHandler());
		
		//Checking all Methods
		Assert.assertTrue(myInterpreter.myStack.isEmpty());
		myInterpreter.readText("Ldc 5");
		assertEquals(5,(int)myInterpreter.myStack.pop());
		Assert.assertTrue(myInterpreter.myStack.isEmpty());
		
		myInterpreter.readText("Ldc 4\n"
				+ "Ldc 3\n"
				+ "Mult");
		assertEquals(12, (int)myInterpreter.myStack.pop());
		Assert.assertTrue(myInterpreter.myStack.empty());
		
		Assert.assertTrue(myInterpreter.register.isEmpty());
		myInterpreter.readText("Ldc 9\n"
				+ "Store y");
		assertEquals(9,(int)myInterpreter.register.get("y"));
		Assert.assertTrue(myInterpreter.myStack.empty());
		
		myInterpreter.readText("Ld y");
		Assert.assertFalse(myInterpreter.myStack.empty());
		assertEquals(9,(int)myInterpreter.myStack.pop());
		
		
		myInterpreter.register.clear(); //Clearing register
		
		myInterpreter.readText("Ldc 6\n"
				+ "Ldc 7\n"
				+ "Mult\n"
				+ "Store x\n"
				+ "Ld x\n"
				+ "Print");
	}
}
