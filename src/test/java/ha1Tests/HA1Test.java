package ha1Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ha1.BurgerBude;
import ha1.DeliveryService;
import ha1.DoenerLaden;
import ha1.FrittenBude;
import ha1.PizzaShop;

/**
 * @author Xavier Ngansop
 * HA1: 10/10
 * Das ist kein Junit-Test. Es fehlen Assertions. Nächstes mal ziehe ich Punkte dadür ab.
 */
public class HA1Test {

	@Test
	public void test() {
		int order = 42;
		String address = "Willi Alle 73";
		DeliveryService myService = new DeliveryService();
		myService.setCaterer(new BurgerBude());
		myService.deliver(order, address);
		
		myService.setCaterer(new PizzaShop());
		myService.deliver(order, address);
		
		myService.setCaterer(new DoenerLaden());
		myService.deliver(order, address);
		
		myService.setCaterer(new FrittenBude());
		myService.deliver(order, address);
		//Neue Zeilen im Test: 3 (mit Import)
		//Neue Zeilen durch Erstellung neuer Klasse 4
	}

}
