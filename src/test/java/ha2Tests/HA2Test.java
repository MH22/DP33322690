package ha2Tests;

import org.junit.Test;

import ha2.Fachbereich;
import ha2.Fachgebiet;
import ha2.Fakultaet;
import ha2.Institut;
import ha2.Personen;
import ha2.Uni;
import junit.framework.Assert;
/**
 * @author Xavier Ngansop
 * HA2: 04/10
 * -4 Visitor Pattern nicht vorhanden
 * -1 Abstrakte Klasse muss abstrakte Methode haben
 * -1 Composite Pattern teilweise richtig
 *
 */
public class HA2Test {
	
	@Test
	public void test() {
		Uni kassel = new Uni("Universit�t Kassel");
		
		Fachbereich fb1 = new Fachbereich("FB1");
		Fachbereich fb16 = new Fachbereich("FB16");
		
		Fachgebiet oekonomie = new Fachgebiet("Oekonomie");
		Fachgebiet unternehmensfuehrung = new Fachgebiet("Unternehmensfuehrung");
		Fachgebiet volkswirtschaft = new Fachgebiet("Volkswirtschaft");
		
		Fachgebiet digitaltechnik = new Fachgebiet("Digitaltechnik");
		Fachgebiet softwareEngineering = new Fachgebiet("SE");
		Fachgebiet verteilteSysteme = new Fachgebiet("Verteilte Systeme");
		
		kassel.setPresi(new Personen("Thomas","Pr�sident"));
		
		Fakultaet ft = new Fakultaet();
		ft.add(fb16);
		ft.add(fb1);
		
		Institut institut16 = new Institut();
		Institut institut16Nr2 = new Institut();
		
		institut16.add(softwareEngineering);
		institut16.add(digitaltechnik);
		institut16Nr2.add(verteilteSysteme);
		
		kassel.add(ft);
		
		fb1.add(oekonomie);
		fb1.add(unternehmensfuehrung);
		fb1.add(volkswirtschaft);
		Personen dk1 = new Personen("Hermann","Dekan");
		fb1.setDekan(dk1);
		
		fb16.add(institut16);
		fb16.add(institut16Nr2);
		Personen dk16 = new Personen("Rudolph","Dekan");
		fb16.setDekan(dk16);
		
		Assert.assertEquals(dk1, fb1.getDekan());
		Assert.assertEquals(dk16, fb16.getDekan());
		
		digitaltechnik.addPerson(new Personen("Schwarz","Prof."));
		digitaltechnik.addPerson(new Personen("Gu�","Prof."));
		digitaltechnik.addPerson(new Personen("Ermann","Doktor."));
		Assert.assertEquals(3,digitaltechnik.countMembers());
		
		volkswirtschaft.addPerson(new Personen("Bachmann","Prof."));
		volkswirtschaft.addPerson(new Personen("Strau�","Prof."));
		volkswirtschaft.addPerson(new Personen("Ermann","Doktor."));
		Assert.assertEquals(3,volkswirtschaft.countMembers());
		
		verteilteSysteme.addPerson(new Personen("Ruth","Prof."));
		verteilteSysteme.addPerson(new Personen("Ernst","Prof."));
		verteilteSysteme.addPerson(new Personen("Erd","Doktor."));
		Assert.assertEquals(3,verteilteSysteme.countMembers());
		
		softwareEngineering.addPerson(new Personen("Hartmann","Prof."));
		softwareEngineering.addPerson(new Personen("Kroos","Doktor."));
		Assert.assertEquals(2,softwareEngineering.countMembers());
		
		oekonomie.addPerson(new Personen("Z�ndorf","Prof."));
		oekonomie.addPerson(new Personen("Copei","Doktor."));
		Assert.assertEquals(2,oekonomie.countMembers());
		
		unternehmensfuehrung.addPerson(new Personen("Hallert","Prof."));
		unternehmensfuehrung.addPerson(new Personen("Mandert","Doktor."));
		Assert.assertEquals(2, unternehmensfuehrung.countMembers());
		
		Assert.assertEquals(8, fb16.countMembers());
		Assert.assertEquals(7, fb1.countMembers());
		Assert.assertEquals(5,institut16.countMembers());
		
		Assert.assertEquals(15, kassel.countMembers());
		
		//Umwelttechnik hinzuf�gen
		Fachgebiet umwelttechnik = new Fachgebiet("Umwelttechnik");
		umwelttechnik.addPerson(new Personen("Heinz","Prof."));
		kassel.add(umwelttechnik);
		Assert.assertEquals(16, kassel.countMembers());
		
		
	}
}
