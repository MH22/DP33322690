package ha6Test;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Stack;

import org.junit.BeforeClass;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import ha6.CloneHandler;
import ha6.CustomGroup;
import ha6.CustomLine;
import ha6.Drawings;
import ha6.Editor;
import ha6.GroupFactory;
import ha6.GroupHandler;
import ha6.LineFactory;
import ha6.UndoHandler;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import junit.framework.Assert;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Line;

// HA6: 10/10
public class ha6Test extends ApplicationTest{
	Editor myEditor;
	
	@SuppressWarnings("restriction")
	@Test
	public void test() {
		clickOn(myEditor.text);
		write("line l1 500 200 550 200").push(KeyCode.ENTER);
		write("line l2 500 200 500 250").push(KeyCode.ENTER);
		write("line l3 550 200 550 250").push(KeyCode.ENTER);
		write("line l4 500 250 550 250").push(KeyCode.ENTER);
		write("line l5 500 200 550 250").push(KeyCode.ENTER);
		write("line l6 550 200 500 250").push(KeyCode.ENTER);
		write("line l7 550 200 525 175").push(KeyCode.ENTER);
		write("line l8 500 200 525 175").push(KeyCode.ENTER);
		
		write("group g1 l1 l2 l3 l4 l5 l6 l7 l8").push(KeyCode.ENTER);
		write("clone g1 g2 100 0").push(KeyCode.ENTER);
		write("clone g1 g3 200 0").push(KeyCode.ENTER);
		
		write("undo").push(KeyCode.ENTER);
		write("undo").push(KeyCode.ENTER);
		write("undo").push(KeyCode.ENTER);
		write("undo").push(KeyCode.ENTER);
		write("undo").push(KeyCode.ENTER);
		write("undo").push(KeyCode.ENTER);
	}
	
	@Override
    public void start(Stage primaryStage) {
    	myEditor = new Editor();
    	myEditor.start(primaryStage);
	}  
	
	
	@Test
	public void testLogic() {
		CustomLine l1 = new CustomLine("name",50.0,60.0,70.0,80.0,myEditor);
		CustomLine l2 = LineFactory.createLine("name", 50.0, 60, 70, 80, myEditor);
		Assert.assertEquals(l1.getEndX(), l2.getEndX());
		Assert.assertEquals(l1.getStartX(), l2.getStartX());
		Assert.assertEquals(l1.getEndY(), l2.getEndY());
		Assert.assertEquals(l1.getStartY(), l2.getStartY());
		
		
		CustomGroup g1 = new CustomGroup();
		g1.setName("name");
		CustomGroup g2 = GroupFactory.createGroup("name");
		
		Assert.assertEquals(g1.getName(), g2.getName());
		
		g1.addDrawings(l1);
		g1.addDrawings(l2);
		
		CustomGroup g3 = (CustomGroup) g1.copyReturn(50, 50);
		for (Drawings drawing: g3.getDrawings()) {
			CustomLine l = (CustomLine) drawing;
			assertEquals(l.getStartX() -50, l1.getStartX(),0.1);
			assertEquals(l.getEndX() -50, l1.getEndX(),0.1);
			assertEquals(l.getStartY() -50, l1.getStartY(),0.1);
			assertEquals(l.getEndY() -50, l1.getEndY(),0.1);
		}
		
	}
	
	
}
